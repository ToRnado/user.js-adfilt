// ToRnado

// ==UserScript==
// @name         geek ysr
// @name:en-US   截拳道
// @description  scriptlets infuse
// @description:en-US DOJO
// @version      0.1
// @author       ToRnado
// @match        *://*/*
// @exclude 	 *://*localhost*
// @exclude 	 *://*127.0.0.1*
// @namespace https://codeberg.org/ToRnado/
// ==/UserScript==

/* scriptlets infuse */
"use strict";

/// tornado.js
/// alias tornado.js

/**********************/
/* Generic Scriptlets */
/**********************/
/// get-url-parameter.js
/// alias gup.js
(function() {
    if (window.location.href.includes("?url=") || window.location.href.includes("&url=")) {
        let urlParams = new URLSearchParams(window.location.search);
        let urlReplacement = urlParams.get("url");
        if (window.location.href.match("url=http")) {
            window.location.replace(urlReplacement);
        } else {
            window.location.replace("https://" + urlReplacement);
        }
    }
})();

/***********************/
/* Specific Scriptlets */
/***********************/
/// amazon-url-cleaner.js
/// alias auc.js
(function() {
    function getProductId() {
        let m;
        m = document.location.href.match(/(?:.+\/)?dp\/([^/?]+)/);
        if (m) {
            return m[1];
        }
        m = document.location.href.match(/gp\/product\/([^/?]+)/);
        if (m) {
            return m[1];
        }
        m = document.location.href.match(/ASIN\/([^/?]+)/);
        if (m) {
            return m[1];
        }
    }
    let productId = getProductId();
    if (productId) {
        history.replaceState({}, document.title, "https://www.amazon.com/dp/" + productId);
    }
})();

/// idcac-fix.js
/// alias idcac.js
import { chromium } from 'playwright'; // works with Firefox too!
import { getInjectableScript } from 'idcac-playwright';

(async () => {
    const b = await chromium.launch({
        headless: false,
    });

    const context = await b.newContext();
    const p = await context.newPage();

    await p.goto('https://google.com');

    // Inject the extension as a client-side script
    await p.evaluate(getInjectableScript());

    // Enjoy your webpage without annoying cookie modals!
})();

/// apple-music-japanese-to-english-album-translator.js
/// alias amjteat.js
(function() {
    if (window.location.href.includes("/music.apple.com/jp/album/")) {
        let oldUrlSearch = window.location.search;
        let urlParams = new URLSearchParams(oldUrlSearch);
        if (urlParams.has("l") === false) {
            if (oldUrlSearch.indexOf("?") === -1) {
                if (!/\?l=en/.test(oldUrlSearch)) {
                    window.location.replace(window.location.protocol + "//" + window.location.host + window.location.pathname + oldUrlSearch + "?l=en-US" + window.location.hash);
                }
            } else {
                if (!/\&l=en/.test(oldUrlSearch)) {
                    window.location.replace(window.location.protocol + "//" + window.location.host + window.location.pathname + oldUrlSearch + "&l=en-US" + window.location.hash);
                }
            }
        }
    }
})();

/// hikarinoakariost-bypasser.js
/// alias hnab.js
(function() {
    if (window.location.href.includes("/hikarinoakari.com/out/")) {
        setTimeout(function() {
            document.querySelector("a[class='link']").click();
        }, 4000);
    }
})();

/// nyaa-dark-mode-enabler.js
/// alias ndme.js
(function() {
    window.addEventListener("DOMContentLoaded", function() {
        if (!document.body.classList.contains("dark")) {
            document.querySelector("a[id='themeToggle']").click();
        }
    });
})();

/// old-reddit-redirector.js
/// alias orr.js
(function() {
    if (window.location.href.includes("/www.reddit.com/") && !window.location.href.includes("/www.reddit.com/gallery/") && !window.location.href.includes("/www.reddit.com/poll/")) {
        window.location.replace(window.location.toString().replace("/www.reddit.com/", "/old.reddit.com/"));
    }
})();

/// ouo-io-bypasser.js
/// alias oib.js
(function() {
    window.addEventListener("load", function() {
        if (document.getElementById("form-captcha") === null) {
            document.getElementsByTagName("form")[0].submit();
        }
        if (document.getElementById("form-captcha").click) {
            document.getElementsByTagName("form")[0].submit();
        }
    });
})();

/// rentry-target-attribute-setter.js
/// alias rtas.js
(function() {
    window.addEventListener("load", function() {
        document.querySelectorAll("a[href^='http']").forEach(function(a) {
            a.setAttribute("target", "_blank");
        });
    });
})();

/// youtube-shorts-redirector.js
/// alias ysr.js
(function() {
    let oldHref = document.location.href;
    if (window.location.href.indexOf("youtube.com/shorts") > -1) {
        window.location.replace(window.location.toString().replace("/shorts/", "/watch?v="));
    }
    window.onload = function() {
        let bodyList = document.querySelector("body");
        let observer = new MutationObserver(function(mutations) {
            mutations.forEach(function() {
                if (oldHref !== document.location.href) {
                    oldHref = document.location.href;
                    if (window.location.href.indexOf("youtube.com/shorts") > -1) {
                        window.location.replace(window.location.toString().replace("/shorts/", "/watch?v="));
                    }
                }
            });
        });
        let config = {
            childList: true,
            subtree: true
        };
        observer.observe(bodyList, config);
    };
})();
